<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Ui\Component\Listing\Column\Mostafamoviesindex;

/**
 * Class PageActions
 * extends \Magento\Ui\Component\Listing\Columns\Column
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["movie_id"])) {
                    $id = $item["movie_id"];
                }
                $item[$name]["edit"] = [
                    "href"=>$this->getContext()->getUrl(
                        "mostafa_movies/movie/edit",
                        ["movie_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
                $item[$name]["delete"] = [
                    "href"=>$this->getContext()->getUrl(
                        "mostafa_movies/movie/delete",
                        ["movie_id"=>$id]
                    ),
                    "label"=>__("Delete"),
                    'confirm' => [
                        'title' => __('Delete Movie'),
                        'message' => __('Are you sure?')
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
