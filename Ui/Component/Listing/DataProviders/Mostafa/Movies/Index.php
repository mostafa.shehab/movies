<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Ui\Component\Listing\DataProviders\Mostafa\Movies;

/**
 * Class Index
 * extends \Magento\Ui\DataProvider\AbstractDataProvider
 */
class Index extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Mostafa\Movies\Model\ResourceModel\Movie\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}
