<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface as Db;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * implements \Magento\Framework\Setup\UpgradeSchemaInterface
 */
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    /**
     * @inheritDoc
     * @throws \Zend_Db_Exception
     */
    public function upgrade(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $tableName = 'mostafa_movies_movie_locale';

        $setup->getConnection()->addForeignKey(
			$setup->getFkName($tableName, 'movie_id', 'mostafa_movies_movie', 'movie_id'),
			$setup->getTable($tableName), 'movie_id', $setup->getTable('mostafa_movies_movie'), 'movie_id', Db::FK_ACTION_CASCADE, false
		);

        //END:   install stuff
        $installer->endSetup();
    }
}
