<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface as Db;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * implements \Magento\Framework\Setup\InstallSchemaInterface
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @inheritDoc
     * @throws \Zend_Db_Exception
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        //START: install stuff
        //END:   install stuff

        //START table setup
        if (!$installer->tableExists('mostafa_movies_movie')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('mostafa_movies_movie')
			)
				->addColumn(
					'movie_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Movie ID'
				)
				->addColumn(
					'movie_title',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable' => false],
					'Movie Name'
				)
				->addColumn(
					'url_key',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Movie URL Key'
				)
				->addColumn(
					'movie_body',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					'64k',
					[],
					'Movie Body'
				)->addColumn(
					'is_active',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					[ 'nullable' => false, 'default' => '1' ],
					'Is Active'
				)->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
					'Created At'
				)->addColumn(
					'updated_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
					'Updated At')
				->setComment('Movie Table');
			$installer->getConnection()->createTable($table);

		}

        $table = $installer->getConnection()->newTable(
            $installer->getTable('mostafa_movies_movie_locale')
        )->addColumn('locale', Table::TYPE_TEXT, 255, [
            'nullable' => true,
        ], 'Locale')->addColumn('movie_id', Table::TYPE_INTEGER, null, [
            'unsigned' => true,
        ], 'Movie ID')->addColumn('title', Table::TYPE_TEXT, 255, [
            'nullable' => true,
        ], 'Movie Title')->addColumn('body', Table::TYPE_TEXT, 255, [
            'nullable' => true,
        ], 'Movie Body')->addColumn('image_url', Table::TYPE_TEXT, 255, [
			'nullable' => true,
		], 'Movie Image');

        $installer->getConnection()->createTable($table);
        //END   table setup
        $installer->endSetup();
    }
}
