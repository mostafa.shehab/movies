<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Model;

/**
 * Class Movie
 * extends \Magento\Framework\Model\AbstractModel
 */
class Movie extends \Magento\Framework\Model\AbstractModel implements
    \Mostafa\Movies\Api\Data\MovieInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'mostafa_movies_movie';

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Mostafa\Movies\Model\ResourceModel\Movie::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getTitle()
    {
        $title = $this->getData('title');
        if ($title === null) {
            $title = $this->getData('movie_title');
        }
        return $title;
    }

    public function getBody()
    {
        $body = $this->getData('body');
        if ($body === null) {
            $body = $this->getData('movie_body');
        }
        return $body;
    }

    public function getImage()
    {
        return $this->getData('image_url');
    }
}
