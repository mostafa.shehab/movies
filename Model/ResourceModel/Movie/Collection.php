<?php

/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Model\ResourceModel\Movie;

use Robusta\Base\Traits\HasLocalization;

/**
 * Class Collection
 * extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    use HasLocalization;

    /**
     * @var string
     */
    private $localeTable;

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Mostafa\Movies\Model\Movie::class, \Mostafa\Movies\Model\ResourceModel\Movie::class);
        $this->localeTable = $this->getTable('mostafa_movies_movie_locale');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $locale = $this->getLocale();
        $this->addBindParam(':movie_locale', $locale);
        $this->getSelect()->joinLeft(
            ['locale_table' => $this->localeTable],
            'main_table.movie_id = locale_table.movie_id AND locale_table.locale = :movie_locale',
            ['locale_table.title', 'locale_table.body', 'locale_table.image_url']
        );
        $this->addFilterToMap('movie_id', 'main_table.movie_id');
        return $this;
    }
}
