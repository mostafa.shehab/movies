<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Model\ResourceModel;

use Robusta\Base\Traits\HasLocalization;

/**
 * Class Movie
 * extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
 */
class Movie extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    use HasLocalization;

    /**
     * @var string
     */
    private $localeTable;

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init('mostafa_movies_movie', 'movie_id');
        $this->localeTable = 'mostafa_movies_movie_locale';
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        $connection = $this->getConnection();
        $locale = $this->getLocale();

        $fieldId = $connection->quoteIdentifier($this->getMainTable() . '.' . $this->getIdFieldName());

        $movieCondition = $connection->quoteInto('locale_table.locale = ?', $locale);
        $select->joinLeft(
            ['locale_table' => $this->localeTable],
            "{$fieldId} = locale_table.movie_id AND {$movieCondition}",
            ['title', 'image_url', 'body']
        );
        return $select;
    }

    public function loadLocaleNames($movieId)
    {
        $sql = $this->getConnection()->select()
            ->from(
                [$this->localeTable],
                ['locale', 'title', 'image_url', 'body']
            )->where("movie_id = " . $movieId);
        return $this->getConnection()->fetchAll($sql);
    }

    public function deleteOldLocales($id)
    {
        $this->getConnection()->delete(
            $this->localeTable,
            ['movie_id = ?' => $id]
        );
    }

    public function saveLocales($movieId, $movieLocaleData)
    {
        if (count($movieLocaleData) == 0) {
            return;
        }
        $inputs = [];
        foreach ($movieLocaleData as $movieLocale) {
            if (isset($movieLocale['image_url'][0]['url'])) {
                $movieLocale['image_url'] = $movieLocale['image_url'][0]['url'];
            }
            $inputs[] = [
                'movie_id' => $movieId,
                'locale' => $movieLocale['locale'],
                'title' => $movieLocale['title'],
                'body' => $movieLocale['body'],
                'image_url' => $movieLocale['image_url'],
            ];
        }
        $this->getConnection()->insertMultiple($this->localeTable, $inputs);
    }
}
