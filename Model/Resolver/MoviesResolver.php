<?php declare(strict_types=1);

namespace Mostafa\Movies\Model\Resolver;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Mostafa\Movies\Api\MovieRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SortOrderBuilder;

class MoviesResolver implements ResolverInterface
{

    private $searchCriteriaBuilder;

    private $moviesRepository;

    private $filterBuilder;

    private $logger;

    private $sortOrderBuilder;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        MovieRepositoryInterface $moviesRepositoryInterface,
        FilterBuilder $filterBuilder,
        LoggerInterface $logger,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->moviesRepository = $moviesRepositoryInterface;
        $this->filterBuilder = $filterBuilder;
        $this->logger = $logger;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    private function filter(array $args)
    {
        $filter = $this->filterBuilder
            ->setField('is_active')
            ->setConditionType('eq')
            ->setValue('1')
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
        
        $this->filterById($args);
        
        $this->filterByTitle($args);

        $this->filterByUrl($args);

        $this->filterByBody($args);   
    }

    private function filterById(array $args)
    {
        if (isset($args['filter']['movie_id']) && $args['filter']['movie_id']) {
            if (isset($args['filter']['movie_id']['from']) && $args['filter']['movie_id']['from']) {
                $filter = $this->filterBuilder
                    ->setField('movie_id')
                    ->setConditionType('from')
                    ->setValue($args['filter']['movie_id']['from'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
            if (isset($args['filter']['movie_id']['to']) && $args['filter']['movie_id']['to']) {
                $filter = $this->filterBuilder
                    ->setField('movie_id')
                    ->setConditionType('to')
                    ->setValue($args['filter']['movie_id']['to'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
    }

    private function filterByTitle(array $args)
    {
        if (isset($args['filter']['movie_title']) && $args['filter']['movie_title']) {
            if (isset($args['filter']['movie_title']['like']) && $args['filter']['movie_title']['like']) {
                $filter = $this->filterBuilder
                    ->setField('movie_title')
                    ->setConditionType('like')
                    ->setValue('%'.$args['filter']['movie_title']['like'].'%')
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            } elseif (isset($args['filter']['movie_title']['eq']) && $args['filter']['movie_title']['eq']) {
                $filter = $this->filterBuilder
                    ->setField('movie_title')
                    ->setConditionType('eq')
                    ->setValue($args['filter']['movie_title']['eq'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
    }

    private function filterByUrl(array $args)
    {
        if (isset($args['filter']['url_key']) && $args['filter']['url_key']) {
            if (isset($args['filter']['url_key']['like']) && $args['filter']['url_key']['like']) {
                $filter = $this->filterBuilder
                    ->setField('url_key')
                    ->setConditionType('like')
                    ->setValue('%'.$args['filter']['url_key']['like'].'%')
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            } elseif (isset($args['filter']['url_key']['eq']) && $args['filter']['url_key']['eq']) {
                $filter = $this->filterBuilder
                    ->setField('url_key')
                    ->setConditionType('eq')
                    ->setValue($args['filter']['url_key']['eq'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
    }

    private function filterByBody(array $args)
    {
        if (isset($args['filter']['movie_body']) && $args['filter']['movie_body']) {
            if (isset($args['filter']['movie_body']['like']) && $args['filter']['movie_body']['like']) {
                $filter = $this->filterBuilder
                    ->setField('movie_body')
                    ->setConditionType('like')
                    ->setValue('%'.$args['filter']['movie_body']['like'].'%')
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            } elseif (isset($args['filter']['movie_body']['eq']) && $args['filter']['movie_body']['eq']) {
                $filter = $this->filterBuilder
                    ->setField('movie_body')
                    ->setConditionType('eq')
                    ->setValue($args['filter']['movie_body']['eq'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
    }

    private function sorting(array $args)
    {
        if (isset($args['sort']['movie_id']) && $args['sort']['movie_id']) {
            $sortOrder = $this->sortOrderBuilder->setField('movie_id')->setDirection($args['sort']['movie_id']);
            $searchCriteria = $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        } elseif (isset($args['sort']['movie_title']) && $args['sort']['movie_title']) {
            $sortOrder = $this->sortOrderBuilder->setField('movie_title')->setDirection($args['sort']['movie_title']);
            $searchCriteria = $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        }
    }

    /**
     * @inheritDoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    { 
        $this->filter($args);

        $this->sorting($args);

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchCriteria->setCurrentPage($args['currentPage']);
        $searchCriteria->setPageSize($args['pageSize']);
        $movieList = $this->moviesRepository->getList($searchCriteria);
        $movies = $movieList->getItems();
        
        return $this->moviesRepository->buildResponseList($movies);
    }
}
