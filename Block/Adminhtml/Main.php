<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Block\Adminhtml;

/**
 * Class Main
 * extends \Magento\Backend\Block\Template
 */
class Main extends \Magento\Backend\Block\Template
{
    public function _prepareLayout()
    {
        return $this;
    }
}
