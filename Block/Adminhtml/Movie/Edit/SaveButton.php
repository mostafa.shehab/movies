<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Block\Adminhtml\Movie\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 * extends GenericButton implements ButtonProviderInterface
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {

        return [
            'label' => __('Save Movie'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
