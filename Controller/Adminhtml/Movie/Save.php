<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Mostafa\Movies\Model\ImageUploader;

/**
 * Class Save
 * extends \Magento\Backend\App\Action
 */
class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE_ID = 'movie_id';
    const ADMIN_RESOURCE_EDIT = 'Mostafa_Movies::edit';
    const ADMIN_RESOURCE_CREATE = 'Mostafa_Movies::create';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Mostafa\Movies\Model\MovieRepository
     */
    protected $objectRepository;

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $file;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $dir;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param ImageUploader $imageUploader
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param \Mostafa\Movies\Model\MovieRepository $objectRepository
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Mostafa\Movies\Model\MovieRepository $objectRepository,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        ImageUploader $imageUploader
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->objectRepository = $objectRepository;
        $this->dir = $dir;
        $this->file = $file;
        $this->imageUploader = $imageUploader;

        parent::__construct($context);
    }

    /**
     * Determines whether current user is allowed to access Action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->getRequest()->getParam(self::ADMIN_RESOURCE_ID)) {
            return $this->_authorization->isAllowed(self::ADMIN_RESOURCE_EDIT);
        }
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE_CREATE);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data['movie_id'])) {
                $data['movie_id'] = null;
            }

            /** @var \Mostafa\Movies\Model\Movie $model */
            $model = $this->_objectManager->create(\Mostafa\Movies\Model\Movie::class);

            $id = $this->getRequest()->getParam('movie_id');
            if ($id) {
                $model = $this->objectRepository->getById($id);
            }

            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the movie.'));
                $this->dataPersistor->clear('mostafa_movies_movie');

                $movieRequestLocaleData = [];
                if (isset($data['locale_values'])) {
                    $movieRequestLocaleData = $data['locale_values'];
                    $this->validateLocaleValues($movieRequestLocaleData);
                }

                $imageFileKey = "image_url";
                $imagesLocalesFiles = $id ? array_map(function ($v) use ($imageFileKey) {
                    return $v[$imageFileKey];
                }, $this->objectRepository->loadLocaleNames($id)) : [];

                $movieRequestLocaleData = [];
                if (isset($data['locale_values'])) {
                    $data["locale_values"] = $this->handleFileCases(
                        $imagesLocalesFiles,
                        $data["locale_values"],
                        'image_url'
                    );
                    $movieRequestLocaleData = $data['locale_values'];
                }

                $this->objectRepository->syncLocales($model->getId(), $movieRequestLocaleData);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['movie_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->dataPersistor->set('mostafa_movies_movie', $data);
            return $resultRedirect->setPath('*/*/edit', ['movie_id' => $this->getRequest()->getParam('movie_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function moveFileFromTmp($name)
    {
        $this->imageUploader->moveFileFromTmp($name);
    }

    /**
     * @param array $oldFiles
     * @param array $data
     * @param string $key
     * @return array $data
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function handleFileCases($oldFiles, $data, $key)
    {
        // Set Movies image urls for each locale
        foreach ($data as $localeKey => $localeData) {
            if (isset($localeData[$key . '_url'][0]['relative_url'])) {
                $this->moveFileFromTmp($localeData[$key . '_url'][0]['name']);
                $data[$localeKey][$key . '_url'] = $localeData[$key . '_url'][0]['relative_url'];
                $oldFileKey = array_search($data[$localeKey][$key . '_url'], $oldFiles);
                if ($oldFileKey !== false) {
                    unset($oldFiles[$oldFileKey]);
                }
            } elseif (isset($localeData[$key . '_url'][0]['url'])) {
                $data[$localeKey][$key . '_url'] = $localeData[$key . '_url'][0]['url'];
                $oldFileKey = array_search($data[$localeKey][$key . '_url'], $oldFiles);
                if ($oldFileKey !== false) {
                    unset($oldFiles[$oldFileKey]);
                }
            } else {
                $data[$localeKey][$key . '_url'] = null;
            }
        }

        foreach ($oldFiles as $file) {
            $this->deleteFile($file);
        }

        return $data;
    }

    /**
     * Delete File
     *
     * @param String $fileUrl
     */
    private function deleteFile($fileUrl)
    {
        if ($fileUrl) {
            $absolute_path = $this->dir->getPath('pub') . $fileUrl;
            if ($this->file->isExists($absolute_path)) {
                $this->file->deleteFile($absolute_path);
            }
        }
    }

    private function validateLocaleValues($locale_values)
    {
        $locales = [];
        foreach ($locale_values as $movie_locale) {
            if (isset($movie_locale['locale']) && !in_array($movie_locale['locale'], $locales)) {
                $locales[] = $movie_locale['locale'];
            } else {
                throw new LocalizedException(__("locale values duplicated"));
            }
        }
    }
}
