<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Controller\Adminhtml\Movie;

/**
 * Class Delete
 * extends \Magento\Backend\App\Action
 */
class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Mostafa_Movies::movie_edit';

    /**
     * @var \Mostafa\Movies\Model\MovieRepository
     */
    protected $objectRepository;

    /**
     * Delete constructor.
     * @param \Mostafa\Movies\Model\MovieRepository $objectRepository
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Mostafa\Movies\Model\MovieRepository $objectRepository,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->objectRepository = $objectRepository;

        parent::__construct($context);
    }

    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('movie_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // delete model
                $this->objectRepository->deleteById($id);
                // display success message
                $this->messageManager->addSuccess(__('You have deleted the Movie.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['movie_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can not find a Movie to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
