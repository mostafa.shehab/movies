<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2020. All Rights Reserved.
 * See README.md for more info
 */

namespace Mostafa\Movies\Api;

use Mostafa\Movies\Api\Data\MovieInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface MovieRepositoryInterface
 *
 * @api
 */
interface MovieRepositoryInterface
{
    /**
     * Create or update a Movie.
     *
     * @param MovieInterface $page
     * @return MovieInterface
     */
    public function save(MovieInterface $page);

    /**
     * Get a Movie by Id
     *
     * @param int $id
     * @return MovieInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Movie with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Movies which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Delete a Movie
     *
     * @param MovieInterface $page
     * @return MovieInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Movie with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(MovieInterface $page);

    /**
     * Delete a Movie by Id
     *
     * @param int $id
     * @return MovieInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
